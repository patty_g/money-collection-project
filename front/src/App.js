import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = { money_list:[] }
 async componentDidMount () {
  try{  
  const response = await fetch('//localhost:8080/money/list')
    const data = await response.json()
    this.setState({money_list:data})
   }catch(err){
    console.log(err)
  }
}
  render() {
    const { money_list } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          { money_list.map( money => 
      <div key={money.id}>
        <p>{money.id} -  {money.currency}</p>
      </div>
  )
}

          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
