import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  /**
   * retrieves the contacts from the database
   */
  const getMoneyList = async () => {
    const rows = await db.all("SELECT money_id AS id, currency, amount, like FROM money")
    return rows
  }

  const controller = {
    getMoneyList
  }

  return controller
}

export default initializeDatabase
