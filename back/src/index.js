import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get('/money/list', async (req, res) => {
    const money_list = await controller.getMoneyList()
    res.json(money_list)
  })
  
  app.listen(8080, () => console.log('server listening on port 8080'))
}
start();
